package com.sarfaraz.urlshortener.model.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class UrlRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private int id;
    @Column(name = "long_url", columnDefinition = "TEXT")
    private String longUrl;
    private String shortUrlDomain;
    private String shortUrl;
    private String shortUrlUniqueCode;

    @OneToOne
    private ParameterRecord parameters;

    private Timestamp createdAt;

    public UrlRecord() {
    }

    public UrlRecord(UrlRecord urlRecord) {

    }

    public UrlRecord(int urlRecordId) {
        this.id = urlRecordId;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLongUrl() {
        return this.longUrl;
    }

    public void setLongUrl(String longUrl) {
        this.longUrl = longUrl;
    }

    public String getShortUrlDomain() {
        return this.shortUrlDomain;
    }

    public void setShortUrlDomain(String shortUrlDomain) {
        this.shortUrlDomain = shortUrlDomain;
    }

    public ParameterRecord getParameters() {
        return this.parameters;
    }

    public void setParameters(ParameterRecord parameters) {
        this.parameters = parameters;
    }

    public Timestamp getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getShortUrl() {
        return this.shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public String getShortUrlUniqueCode() {
        return this.shortUrlUniqueCode;
    }

    public void setShortUrlUniqueCode(String shortUrlUniqueCode) {
        this.shortUrlUniqueCode = shortUrlUniqueCode;
    }

}