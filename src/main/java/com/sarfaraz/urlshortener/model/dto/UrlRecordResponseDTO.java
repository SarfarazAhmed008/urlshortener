package com.sarfaraz.urlshortener.model.dto;

public class UrlRecordResponseDTO {
    private String short_url;
    private int unique_id;

    public String getShort_url() {
        return this.short_url;
    }

    public void setShort_url(String short_url) {
        this.short_url = short_url;
    }

    public int getUnique_id() {
        return this.unique_id;
    }

    public void setUnique_id(int unique_id) {
        this.unique_id = unique_id;
    }

}
