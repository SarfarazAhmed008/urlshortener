package com.sarfaraz.urlshortener.model.dto;

public class UrlRecordDTO {
    private String long_url;
    private String short_url_domain;

    private ParameterRecordDTO parameters;

    public String getLong_url() {
        return this.long_url;
    }

    public void setLong_url(String long_url) {
        this.long_url = long_url;
    }

    public String getShort_url_domain() {
        return this.short_url_domain;
    }

    public void setShort_url_domain(String short_url_domain) {
        this.short_url_domain = short_url_domain;
    }

    public ParameterRecordDTO getParameters() {
        return this.parameters;
    }

    public void setParameters(ParameterRecordDTO parameters) {
        this.parameters = parameters;
    }

}