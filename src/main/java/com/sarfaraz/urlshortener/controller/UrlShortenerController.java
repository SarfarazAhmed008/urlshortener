package com.sarfaraz.urlshortener.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sarfaraz.urlshortener.model.dto.UrlRecordDTO;
import com.sarfaraz.urlshortener.model.dto.UrlRecordResponseDTO;
import com.sarfaraz.urlshortener.model.entity.ParameterRecord;
import com.sarfaraz.urlshortener.model.entity.UrlRecord;
import com.sarfaraz.urlshortener.model.entity.UserClickInfo;
import com.sarfaraz.urlshortener.repository.ParameterRecordRepository;
import com.sarfaraz.urlshortener.repository.UrlRecordRepository;
import com.sarfaraz.urlshortener.repository.UserClickInfoRepository;
import com.sarfaraz.urlshortener.util.URLName;
import com.sarfaraz.urlshortener.util.Utility;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UrlShortenerController {

    @Autowired
    UrlRecordRepository urlRecordRepository;

    @Autowired
    ParameterRecordRepository parameterRecordRepository;

    @Autowired
    UserClickInfoRepository userClickInfoRepository;

    @Autowired
    Utility utility;

    @PostMapping(path = URLName.STORE_RECORD)
    public String storeRecord(HttpServletRequest request) {

        String shortUrl = "";
        String uniqueCode = "";
        try {
            UrlRecordDTO urlRecordDTO = utility.parseJsonToObjects(request);

            UrlRecord urlRecord = new UrlRecord();
            urlRecord.setLongUrl(urlRecordDTO.getLong_url());
            urlRecord.setShortUrlDomain(urlRecordDTO.getShort_url_domain());
            urlRecord.setCreatedAt(new Timestamp(System.currentTimeMillis()));

            ParameterRecord parameterRecord = new ParameterRecord();
            parameterRecord.setParam1(urlRecordDTO.getParameters().getParam1());
            parameterRecord.setParam2(urlRecordDTO.getParameters().getParam2());

            urlRecord.setParameters(parameterRecord);

            uniqueCode = utility.generateUniqueCode(6);
            shortUrl = utility.generateShortUrl(urlRecordDTO.getShort_url_domain(), uniqueCode);
            urlRecord.setShortUrl(shortUrl);
            urlRecord.setShortUrlUniqueCode(uniqueCode);

            parameterRecordRepository.saveAndFlush(parameterRecord);
            UrlRecord urlRecordDB = urlRecordRepository.save(urlRecord);

            UrlRecordResponseDTO urlRecordResponseDTO = new UrlRecordResponseDTO();
            urlRecordResponseDTO.setUnique_id(urlRecordDB.getId());
            urlRecordResponseDTO.setShort_url(shortUrl);

            String urlRecordResponseJSON = "";
            try {
                urlRecordResponseJSON = new ObjectMapper().writeValueAsString(urlRecordResponseDTO);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            return urlRecordResponseJSON;

        } catch (Throwable e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return "500";

    }

    @GetMapping(path = URLName.RECORD_BY_ID)
    public String getRecordById(@RequestParam int id) {
        UrlRecord urlRecord = urlRecordRepository.getUrlRecordById(id);
        if (urlRecord == null) {
            return new String();
        }
        String urlRecordResponseJSON = "";
        try {
            urlRecordResponseJSON = new ObjectMapper().writeValueAsString(urlRecord);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return urlRecordResponseJSON;
    }

    @GetMapping(path = URLName.SHORT_URL_ID)
    public void getURLValue(@PathVariable String shortUrlId, HttpServletRequest request, HttpServletResponse response) {
        String longUrl = "";
        UrlRecord urlRecord = urlRecordRepository.getUrlRecordByShortUrlUniqueCode(shortUrlId);
        if (urlRecord == null) {
            return;
        }
    
        Map<String, String> headerFile = Utility.getRequestHeaderData(request);

        String host = headerFile.get("host");
        String origin = headerFile.get("origin");
        String userAgent = headerFile.get("user-agent");
        String referer = headerFile.get("referer");
        String cookie = headerFile.get("cookie");

        UserClickInfo userClickInfo = new UserClickInfo();
        userClickInfo.setHost(host);
        userClickInfo.setCookie(cookie);
        userClickInfo.setOrigin(origin);
        userClickInfo.setReferer(referer);
        userClickInfo.setUserAgent(userAgent);
        userClickInfo.setUrlRecord(new UrlRecord(urlRecord.getId()));
        userClickInfo.setCreatedAt(new Timestamp(System.currentTimeMillis()));
        userClickInfo.setIpAddress(request.getRemoteAddr());

        userClickInfoRepository.saveAndFlush(userClickInfo);

        if (urlRecord.getLongUrl().contains("?")) {
            longUrl = urlRecord.getLongUrl() + "&param1=" + urlRecord.getParameters().getParam1() + "&param2="
                    + urlRecord.getParameters().getParam2();
        } else {
            longUrl = urlRecord.getLongUrl() + "?param1=" + urlRecord.getParameters().getParam1() + "&param2="
                    + urlRecord.getParameters().getParam2();
        }

        try {
            response.sendRedirect(longUrl);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @GetMapping(path = URLName.ALL_USER_CLICKED_DATA)
    public String getAllUserClickedData() {
        List<UserClickInfo> userClickInfoList = userClickInfoRepository.getAllUserClickInfo();

        if (userClickInfoList.size() == 0) {
            return new String();
        }

        String userClickInfoJSON = "";
        try {
            userClickInfoJSON = new ObjectMapper().writeValueAsString(userClickInfoList);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return userClickInfoJSON;
    }

    @GetMapping(path = URLName.TOTAL_COUNT_SHORT_URL)
    public int getTotalCountShortUrl() {
        int shortUrlCount = urlRecordRepository.totalShortUrlCount();
        return shortUrlCount;
    }

    @GetMapping(path = URLName.ROOT)
    public String root() {
        return "Hello";
    }

}
