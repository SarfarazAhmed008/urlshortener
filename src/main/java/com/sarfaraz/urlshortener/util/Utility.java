package com.sarfaraz.urlshortener.util;

import java.io.BufferedReader;
import java.security.SecureRandom;
import java.util.Enumeration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;

import com.google.gson.Gson;
import com.sarfaraz.urlshortener.model.dto.UrlRecordDTO;

import org.springframework.stereotype.Component;

@Component
public class Utility {

    public UrlRecordDTO parseJsonToObjects(HttpServletRequest request) {
        StringBuffer stringBuffer = new StringBuffer();
        BufferedReader bufferedReader;
        String line = "";
        try {
            bufferedReader = request.getReader();

            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
            }

            Gson gson = new Gson();
            UrlRecordDTO urlRecordDTO = gson.fromJson(stringBuffer.toString(), UrlRecordDTO.class);
            return urlRecordDTO;

        } catch (Throwable e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return new UrlRecordDTO();
    }

    public String generateUniqueCode(int length) {
        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        SecureRandom rnd = new SecureRandom();

        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();

    }

    public String generateShortUrl(String urlDomain, String uniqueCode) {
        return "http://" + urlDomain + "/" + uniqueCode;
    }

    public static Map<String, String> getRequestHeaderData(HttpServletRequest request) {
        Map<String, String> headerFileMap = new ConcurrentHashMap<String, String>();
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            headerFileMap.put(key, value);
        }
        return headerFileMap;
    }

}
