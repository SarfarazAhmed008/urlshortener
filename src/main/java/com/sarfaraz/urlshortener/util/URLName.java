package com.sarfaraz.urlshortener.util;

public class URLName {
    public final static String STORE_RECORD = "/storeRecord";
    public final static String RECORD_BY_ID = "/getRecordById";
    public final static String SHORT_URL_ID = "/{shortUrlId}";
    public final static String ALL_USER_CLICKED_DATA = "/getAllUserClickedData";
    public final static String TOTAL_COUNT_SHORT_URL = "/getTotalCountShortUrl";

    public final static String ROOT = "/";
}
