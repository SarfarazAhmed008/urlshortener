package com.sarfaraz.urlshortener.repository;

import com.sarfaraz.urlshortener.model.entity.ParameterRecord;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ParameterRecordRepository extends JpaRepository<ParameterRecord, Integer> {

}