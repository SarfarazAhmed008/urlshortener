package com.sarfaraz.urlshortener.repository;

import com.sarfaraz.urlshortener.model.entity.UrlRecord;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UrlRecordRepository extends JpaRepository<UrlRecord, Integer> {

	@Query("SELECT urlRecord FROM UrlRecord urlRecord WHERE urlRecord.id = :id")
	public UrlRecord getUrlRecordById(@Param("id") int id);

	@Query("SELECT urlRecord FROM UrlRecord urlRecord WHERE urlRecord.shortUrlUniqueCode = :shortUrlUniqueCode")
	public UrlRecord getUrlRecordByShortUrlUniqueCode(@Param("shortUrlUniqueCode") String shortUrlUniqueCode);

	@Query("SELECT COUNT(urlRecord) FROM UrlRecord urlRecord")
	public int totalShortUrlCount();

}