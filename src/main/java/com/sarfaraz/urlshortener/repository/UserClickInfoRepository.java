package com.sarfaraz.urlshortener.repository;

import java.util.List;

import com.sarfaraz.urlshortener.model.entity.UserClickInfo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserClickInfoRepository extends JpaRepository<UserClickInfo, Integer> {

    @Query("Select userClick from UserClickInfo userClick")
    public List<UserClickInfo> getAllUserClickInfo();
}