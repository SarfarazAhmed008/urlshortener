## Url Shortener

To run this project please clone this repository. Create a mysql database and name it "urlshortener".

### API Documentation
To store record, send a post request in this endpoint (you can use Postman to do that).

``
http://localhost:8080/storeRecord
``

You need to pass data in this request as json format and header **Content-Type** should be **application/json**

For example, 
if we pass the following data as POST request to this endpoint

```json
{
	"long_url": "http://example.com/path1/path2/path3?arg=value&arg1=value",
	"short_url_domain": "localhost:8080",
	"parameters": {
	"param1": "value1",
	"param2": "value2"
	}
}
```
We will get the following output as json format from this endpoint (on success)

```json
{
    "short_url": "http://localhost:8080/JRMo3o",
    "unique_id": 1
}
```

To get the url record by id, send a GET request using the following endpoint,

For example:

``
http://localhost:8080/getRecordById?id=1
``

If we send a GET request using this URL then we will get a response back as a json format
```json
{
    "id": 1,
    "longUrl": "http://example.com/path1/path2/path3?arg=value&arg1=value",
    "shortUrlDomain": "localhost:8080",
    "shortUrl": "http://localhost:8080/JRMo3o",
    "shortUrlUniqueCode": "JRMo3o",
    "parameters": {
        "id": 1,
        "param1": "value1",
        "param2": "value2"
    },
    "createdAt": 1617283494000
}
```
Upon clicking a **shortUrl**, it will redirect you to the original long url. System will store some user information.

To get the all user clicked data. Send a GET request in the following endpoint

``
http://localhost:8080/getAllUserClickedData
``

It will response back with all the user clicked data as JSON format like below

```json
[
    {
        "id": 1,
        "host": "localhost:8080",
        "origin": null,
        "userAgent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36",
        "referer": null,
        "cookie": null,
        "ipAddress": "0:0:0:0:0:0:0:1",
        "urlRecord": {
            "id": 1,
            "longUrl": "http://example.com/path1/path2/path3?arg=value&arg1=value",
            "shortUrlDomain": "localhost:8080",
            "shortUrl": "http://localhost:8080/JRMo3o",
            "shortUrlUniqueCode": "JRMo3o",
            "parameters": {
                "id": 1,
                "param1": "value1",
                "param2": "value2"
            },
            "createdAt": 1617283494000
        },
        "createdAt": 1617285367000
    },
    {
        "id": 2,
        "host": "localhost:8080",
        "origin": null,
        "userAgent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36",
        "referer": null,
        "cookie": null,
        "ipAddress": "0:0:0:0:0:0:0:1",
        "urlRecord": {
            "id": 1,
            "longUrl": "http://example.com/path1/path2/path3?arg=value&arg1=value",
            "shortUrlDomain": "localhost:8080",
            "shortUrl": "http://localhost:8080/JRMo3o",
            "shortUrlUniqueCode": "JRMo3o",
            "parameters": {
                "id": 1,
                "param1": "value1",
                "param2": "value2"
            },
            "createdAt": 1617283494000
        },
        "createdAt": 1617285371000
    }
]
```
To get total count on short url, send a GET request to the following endpoint

``
http://localhost:8080/getTotalCountShortUrl
``

It will response back the total count

``
1
``


Thank you